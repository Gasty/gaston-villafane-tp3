﻿# Trabajo práctico 3
#### Alumno: 
Gastón Villfañe 
#### Docentes:
Rene Cejas Bolecek / Martín Rene
#### Tema del trabajo practico:
Uso de una terminal.
#### Objetivos:
- Familiarizarse con la utilización de la terminal y comandos de Linux (LNX) Shell.
- Realizar backups sencillos de archivos para resguardar datos.
- Fortalecer los conocimientos en el empleo de la herramienta GIT.

#### Aclaraciónes :
- Los ejercicios del trabajo practico seran relizados a travez de la terminal "gitbash", ya que no quise instalar el sistema operativo linux, contando con windows. 
- Solo seran escritos los comandos en un archivo de texto, previamente comprobados en dicha terminal. 
- La carpeta donde se encuentran los archivos que van a ser modificados para la actividad, está en "C:\Users\nombre\Desktop\tp3-data-master", es decir, en mi escritorio en una carpeta llamada "tp3-data-master", el cual es el nombre con el que se descargaron los archivos para la actividad.
- Usaremos el símbolo "$" para aclarar que sé está escribiendo un comando y la salida de la terminal con el símbolo ">". También usaremos "()" para aclarar lo que sé está haciendo.

## Ejercicio 1.c)  A:
##### 1.  (Inicio la terminal "gibash" y comenzar a agregar comandos)

##### 2. (para saber donde estoy)

. $pwd

. > /c/Users/nombre 

##### 3. (para ir a la carpeta donde están los archivos) 

. $cd desktop/tp3-data-master/fake

. >

##### 4. (para comprobar que estoy donde están los archivos)

. $ls

.> mid_279_1111.jpg  mid_426_1111.jpg  mid_447_0011.jpg  mid_464_1111.jpg
mid_297_0010.jpg  mid_428_0001.jpg  mid_448_0011.jpg  mid_466_1111.jpg
mid_309_1111.jpg  mid_429_1110.jpg  mid_449_1111.jpg  mid_467_1111.jpg
mid_317_1101.jpg  mid_431_0001.jpg  mid_452_1111.jpg  mid_470_1001.jpg
mid_318_1111.jpg  mid_432_1111.jpg  mid_453_1111.jpg  mid_471_1111.jpg
mid_320_1111.jpg  mid_434_1110.jpg  mid_454_1111.jpg  mid_473_0011.jpg
mid_324_1111.jpg  mid_435_1111.jpg  mid_455_0101.jpg  mid_474_1110.jpg
mid_343_1111.jpg  mid_436_1111.jpg  mid_456_1111.jpg  mid_476_1111.jpg
mid_349_1111.jpg  mid_437_1101.jpg  mid_458_1111.jpg  mid_477_1100.jpg
mid_399_0011.jpg  mid_441_0011.jpg  mid_459_1001.jpg  mid_479_1111.jpg
mid_423_1101.jpg  mid_442_0111.jpg  mid_460_1111.jpg  mid_480_1111.jpg
mid_424_1111.jpg  mid_444_1111.jpg  mid_461_1100.jpg
mid_425_1111.jpg  mid_446_0011.jpg  mid_462_1101.jpg

##### 5. (para cambiar el prefijo "mid" por "img")

. $for imagenes in mid*	

. $do

. $mv "$imagenes" "$(echo "$imagenes"|sed "s/^mid/img/g")"

. $done

##### 6. (para comprobar si funciono)

. $ls

. > img_279_1111.jpg  img_426_1111.jpg  img_447_0011.jpg  img_464_1111.jpg
img_297_0010.jpg  img_428_0001.jpg  img_448_0011.jpg  img_466_1111.jpg
img_309_1111.jpg  img_429_1110.jpg  img_449_1111.jpg  img_467_1111.jpg
img_317_1101.jpg  img_431_0001.jpg  img_452_1111.jpg  img_470_1001.jpg
img_318_1111.jpg  img_432_1111.jpg  img_453_1111.jpg  img_471_1111.jpg
img_320_1111.jpg  img_434_1110.jpg  img_454_1111.jpg  img_473_0011.jpg
img_324_1111.jpg  img_435_1111.jpg  img_455_0101.jpg  img_474_1110.jpg
img_343_1111.jpg  img_436_1111.jpg  img_456_1111.jpg  img_476_1111.jpg
img_349_1111.jpg  img_437_1101.jpg  img_458_1111.jpg  img_477_1100.jpg
img_399_0011.jpg  img_441_0011.jpg  img_459_1001.jpg  img_479_1111.jpg
img_423_1101.jpg  img_442_0111.jpg  img_460_1111.jpg  img_480_1111.jpg
img_424_1111.jpg  img_444_1111.jpg  img_461_1100.jpg
img_425_1111.jpg  img_446_0011.jpg  img_462_1101.jpg

##### 7. (Fin del ejercicio)


## Ejercicio 1.c)  B:
##### 1. (veo el estado de los archivos)

. $ls

. > mid_349_1111.jpg  mid_432_1111.jpg  mid_447_0011.jpg  mid_459_1001.jpg  mid_473_0011.jpg
mid_279_1111.jpg  mid_399_0011.jpg  mid_434_1110.jpg  mid_448_0011.jpg  mid_460_1111.jpg  mid_474_1110.jpg
mid_297_0010.jpg  mid_423_1101.jpg  mid_435_1111.jpg  mid_449_1111.jpg  mid_461_1100.jpg  mid_476_1111.jpg
mid_309_1111.jpg  mid_424_1111.jpg  mid_436_1111.jpg  mid_452_1111.jpg  mid_462_1101.jpg  mid_477_1100.jpg
mid_317_1101.jpg  mid_425_1111.jpg  mid_437_1101.jpg  mid_453_1111.jpg  mid_464_1111.jpg  mid_479_1111.jpg
mid_318_1111.jpg  mid_426_1111.jpg  mid_441_0011.jpg  mid_454_1111.jpg  mid_466_1111.jpg  mid_480_1111.jpg
mid_320_1111.jpg  mid_428_0001.jpg  mid_442_0111.jpg  mid_455_0101.jpg  mid_467_1111.jpg
mid_324_1111.jpg  mid_429_1110.jpg  mid_444_1111.jpg  mid_456_1111.jpg  mid_470_1001.jpg
mid_343_1111.jpg  mid_431_0001.jpg  mid_446_0011.jpg  mid_458_1111.jpg  mid_471_1111.jpg

##### 2. (una vez comprobado, decido usar "--" para separar el nombre antiguo del nuevo; también elijo como variable la letra "f" así simplifico todo; compruebo la respuesta de la terminal)

. $for f in mid*

. $do

. $echo "$f" "--" "$(echo "$f"|sed "s/^mid/img/g")"

. $done

. >mid_279_1111.jpg -- img_279_1111.jpg

mid_297_0010.jpg -- img_297_0010.jpg

mid_309_1111.jpg -- img_309_1111.jpg

mid_317_1101.jpg -- img_317_1101.jpg

mid_318_1111.jpg -- img_318_1111.jpg

mid_320_1111.jpg -- img_320_1111.jpg

mid_324_1111.jpg -- img_324_1111.jpg

mid_343_1111.jpg -- img_343_1111.jpg

mid_349_1111.jpg -- img_349_1111.jpg

mid_399_0011.jpg -- img_399_0011.jpg

mid_423_1101.jpg -- img_423_1101.jpg

mid_424_1111.jpg -- img_424_1111.jpg

mid_425_1111.jpg -- img_425_1111.jpg

mid_426_1111.jpg -- img_426_1111.jpg

mid_428_0001.jpg -- img_428_0001.jpg

mid_429_1110.jpg -- img_429_1110.jpg

mid_431_0001.jpg -- img_431_0001.jpg

mid_432_1111.jpg -- img_432_1111.jpg

mid_434_1110.jpg -- img_434_1110.jpg

mid_435_1111.jpg -- img_435_1111.jpg

mid_436_1111.jpg -- img_436_1111.jpg

mid_437_1101.jpg -- img_437_1101.jpg

mid_441_0011.jpg -- img_441_0011.jpg

mid_442_0111.jpg -- img_442_0111.jpg

mid_444_1111.jpg -- img_444_1111.jpg

mid_446_0011.jpg -- img_446_0011.jpg

mid_447_0011.jpg -- img_447_0011.jpg

mid_448_0011.jpg -- img_448_0011.jpg

mid_449_1111.jpg -- img_449_1111.jpg

mid_452_1111.jpg -- img_452_1111.jpg

mid_453_1111.jpg -- img_453_1111.jpg

mid_454_1111.jpg -- img_454_1111.jpg

mid_455_0101.jpg -- img_455_0101.jpg

mid_456_1111.jpg -- img_456_1111.jpg

mid_458_1111.jpg -- img_458_1111.jpg

mid_459_1001.jpg -- img_459_1001.jpg

mid_460_1111.jpg -- img_460_1111.jpg

mid_461_1100.jpg -- img_461_1100.jpg

mid_462_1101.jpg -- img_462_1101.jpg

mid_464_1111.jpg -- img_464_1111.jpg

mid_466_1111.jpg -- img_466_1111.jpg

mid_467_1111.jpg -- img_467_1111.jpg

mid_470_1001.jpg -- img_470_1001.jpg

mid_471_1111.jpg -- img_471_1111.jpg

mid_473_0011.jpg -- img_473_0011.jpg

mid_474_1110.jpg -- img_474_1110.jpg

mid_476_1111.jpg -- img_476_1111.jpg

mid_477_1100.jpg -- img_477_1100.jpg

mid_479_1111.jpg -- img_479_1111.jpg

mid_480_1111.jpg -- img_480_1111.jpg

##### 3. (una vez confirmado el "formato" con el que se ven los nombres, cambio la salida para que en lugar de la terminal, envíe la información aun archivo .dat)

. $for f in mid*

. $do

. $echo "$f" "--" "$(echo "$f"|sed "s/^mid/img/g")" >> renameFake.dat

. $done

##### 4. (compruebo que existe el archivo)

. $ls
. >mid_279_1111.jpg  mid_399_0011.jpg  mid_434_1110.jpg  mid_448_0011.jpg  mid_460_1111.jpg  mid_474_1110.jpg
mid_297_0010.jpg  mid_423_1101.jpg  mid_435_1111.jpg  mid_449_1111.jpg  mid_461_1100.jpg  mid_476_1111.jpg
mid_309_1111.jpg  mid_424_1111.jpg  mid_436_1111.jpg  mid_452_1111.jpg  mid_462_1101.jpg  mid_477_1100.jpg
mid_317_1101.jpg  mid_425_1111.jpg  mid_437_1101.jpg  mid_453_1111.jpg  mid_464_1111.jpg  mid_479_1111.jpg
mid_318_1111.jpg  mid_426_1111.jpg  mid_441_0011.jpg  mid_454_1111.jpg  mid_466_1111.jpg  mid_480_1111.jpg
mid_320_1111.jpg  mid_428_0001.jpg  mid_442_0111.jpg  mid_455_0101.jpg  mid_467_1111.jpg  renameFake.dat
mid_324_1111.jpg  mid_429_1110.jpg  mid_444_1111.jpg  mid_456_1111.jpg  mid_470_1001.jpg
mid_343_1111.jpg  mid_431_0001.jpg  mid_446_0011.jpg  mid_458_1111.jpg  mid_471_1111.jpg
mid_349_1111.jpg  mid_432_1111.jpg  mid_447_0011.jpg  mid_459_1001.jpg  mid_473_0011.jpg

##### 5. (como el archivo si esta creado, compruebo que el halla funcionado)

. $cat renameFake.dat

. >mid_279_1111.jpg -- img_279_1111.jpg

mid_297_0010.jpg -- img_297_0010.jpg

mid_309_1111.jpg -- img_309_1111.jpg

mid_317_1101.jpg -- img_317_1101.jpg

mid_318_1111.jpg -- img_318_1111.jpg

mid_320_1111.jpg -- img_320_1111.jpg

mid_324_1111.jpg -- img_324_1111.jpg

mid_343_1111.jpg -- img_343_1111.jpg

mid_349_1111.jpg -- img_349_1111.jpg

mid_399_0011.jpg -- img_399_0011.jpg

mid_423_1101.jpg -- img_423_1101.jpg

mid_424_1111.jpg -- img_424_1111.jpg

mid_425_1111.jpg -- img_425_1111.jpg

mid_426_1111.jpg -- img_426_1111.jpg

mid_428_0001.jpg -- img_428_0001.jpg

mid_429_1110.jpg -- img_429_1110.jpg

mid_431_0001.jpg -- img_431_0001.jpg

mid_432_1111.jpg -- img_432_1111.jpg

mid_434_1110.jpg -- img_434_1110.jpg

mid_435_1111.jpg -- img_435_1111.jpg

mid_436_1111.jpg -- img_436_1111.jpg

mid_437_1101.jpg -- img_437_1101.jpg

mid_441_0011.jpg -- img_441_0011.jpg

mid_442_0111.jpg -- img_442_0111.jpg

mid_444_1111.jpg -- img_444_1111.jpg

mid_446_0011.jpg -- img_446_0011.jpg

mid_447_0011.jpg -- img_447_0011.jpg

mid_448_0011.jpg -- img_448_0011.jpg

mid_449_1111.jpg -- img_449_1111.jpg

mid_452_1111.jpg -- img_452_1111.jpg

mid_453_1111.jpg -- img_453_1111.jpg

mid_454_1111.jpg -- img_454_1111.jpg

mid_455_0101.jpg -- img_455_0101.jpg

mid_456_1111.jpg -- img_456_1111.jpg

mid_458_1111.jpg -- img_458_1111.jpg

mid_459_1001.jpg -- img_459_1001.jpg

mid_460_1111.jpg -- img_460_1111.jpg

mid_461_1100.jpg -- img_461_1100.jpg

mid_462_1101.jpg -- img_462_1101.jpg

mid_464_1111.jpg -- img_464_1111.jpg

mid_466_1111.jpg -- img_466_1111.jpg

mid_467_1111.jpg -- img_467_1111.jpg

mid_470_1001.jpg -- img_470_1001.jpg

mid_471_1111.jpg -- img_471_1111.jpg

mid_473_0011.jpg -- img_473_0011.jpg

mid_474_1110.jpg -- img_474_1110.jpg

mid_476_1111.jpg -- img_476_1111.jpg

mid_477_1100.jpg -- img_477_1100.jpg

mid_479_1111.jpg -- img_479_1111.jpg

mid_480_1111.jpg -- img_480_1111.jpg

##### 6. (fin del ejercicio)

## Ejercicio 1.c)  C:
##### 1. (me muevo a la carpeta "real")

. $cd ..

. $cd real

.$ pwd

. > /c/Users/nombre/desktop/tp3-data-master/real

##### 2. (compruebo los archivos de la carpeta)

. $ls

. >real_00061.jpg  real_00069.jpg  real_00078.jpg  real_00087.jpg  real_00095.jpg  real_00104.jpg
real_00062.jpg  real_00070.jpg  real_00079.jpg  real_00088.jpg  real_00096.jpg  real_00106.jpg
real_00063.jpg  real_00071.jpg  real_00080.jpg  real_00089.jpg  real_00097.jpg  real_00107.jpg
real_00064.jpg  real_00072.jpg  real_00081.jpg  real_00090.jpg  real_00098.jpg  real_00108.jpg
real_00065.jpg  real_00074.jpg  real_00082.jpg  real_00091.jpg  real_00100.jpg  real_00109.jpg
real_00066.jpg  real_00075.jpg  real_00083.jpg  real_00092.jpg  real_00101.jpg  real_00111.jpg
real_00067.jpg  real_00076.jpg  real_00084.jpg  real_00093.jpg  real_00102.jpg  real_00113.jpg
real_00068.jpg  real_00077.jpg  real_00085.jpg  real_00094.jpg  real_00103.jpg

##### 3. (nuevamente uso el loop para cambiar los nombres; uso "f" como variable para no complicar las cosas)

. $for f in real*

. $do

. $mv "$f" "$(echo "$f"|sed "s/^real/img/g")"

. $done

##### 4. (compruebo los cambios)

. $ls 

. >img_00061.jpg  img_00068.jpg  img_00076.jpg  img_00083.jpg  img_00091.jpg  img_00098.jpg  img_00107.jpg
img_00062.jpg  img_00069.jpg  img_00077.jpg  img_00084.jpg  img_00092.jpg  img_00100.jpg  img_00108.jpg
img_00063.jpg  img_00070.jpg  img_00078.jpg  img_00085.jpg  img_00093.jpg  img_00101.jpg  img_00109.jpg
img_00064.jpg  img_00071.jpg  img_00079.jpg  img_00087.jpg  img_00094.jpg  img_00102.jpg  img_00111.jpg
img_00065.jpg  img_00072.jpg  img_00080.jpg  img_00088.jpg  img_00095.jpg  img_00103.jpg  img_00113.jpg
img_00066.jpg  img_00074.jpg  img_00081.jpg  img_00089.jpg  img_00096.jpg  img_00104.jpg
img_00067.jpg  img_00075.jpg  img_00082.jpg  img_00090.jpg  img_00097.jpg  img_00106.jpg

##### 5. (fin del ejercicio)

##### 1. (compruebo los archivos en la carpeta "real")

. $ls

. > real_00061.jpg  real_00071.jpg  real_00082.jpg  real_00093.jpg  real_00104.jpg
real_00062.jpg  real_00072.jpg  real_00083.jpg  real_00094.jpg  real_00106.jpg
real_00063.jpg  real_00074.jpg  real_00084.jpg  real_00095.jpg  real_00107.jpg
real_00064.jpg  real_00075.jpg  real_00085.jpg  real_00096.jpg  real_00108.jpg
real_00065.jpg  real_00076.jpg  real_00087.jpg  real_00097.jpg  real_00109.jpg
real_00066.jpg  real_00077.jpg  real_00088.jpg  real_00098.jpg  real_00111.jpg
real_00067.jpg  real_00078.jpg  real_00089.jpg  real_00100.jpg  real_00113.jpg
real_00068.jpg  real_00079.jpg  real_00090.jpg  real_00101.jpg
real_00069.jpg  real_00080.jpg  real_00091.jpg  real_00102.jpg
real_00070.jpg  real_00081.jpg  real_00092.jpg  real_00103.jpg

##### 2. (genero el archivo con el formato de igual manera que en el ejercicio anterior)

. $for f in real*

. $do

. $echo "$f" "--" "$(echo "$f"|sed "s/^real/img/g")" >> renameReal.dat

.$done

##### 3. (compruebo que el archivo exista)

. $ls
. >real_00061.jpg  real_00071.jpg  real_00082.jpg  real_00093.jpg  real_00104.jpg
real_00062.jpg  real_00072.jpg  real_00083.jpg  real_00094.jpg  real_00106.jpg
real_00063.jpg  real_00074.jpg  real_00084.jpg  real_00095.jpg  real_00107.jpg
real_00064.jpg  real_00075.jpg  real_00085.jpg  real_00096.jpg  real_00108.jpg
real_00065.jpg  real_00076.jpg  real_00087.jpg  real_00097.jpg  real_00109.jpg
real_00066.jpg  real_00077.jpg  real_00088.jpg  real_00098.jpg  real_00111.jpg
real_00067.jpg  real_00078.jpg  real_00089.jpg  real_00100.jpg  real_00113.jpg
real_00068.jpg  real_00079.jpg  real_00090.jpg  real_00101.jpg  renameReal.dat
real_00069.jpg  real_00080.jpg  real_00091.jpg  real_00102.jpg
real_00070.jpg  real_00081.jpg  real_00092.jpg  real_00103.jpg

##### 4. (como comprobé que el archivo se creo, verifico el resultado)

. $cat renameReal.dat 

. >real_00061.jpg -- img_00061.jpg

real_00062.jpg -- img_00062.jpg

real_00063.jpg -- img_00063.jpg

real_00064.jpg -- img_00064.jpg

real_00065.jpg -- img_00065.jpg

real_00066.jpg -- img_00066.jpg

real_00067.jpg -- img_00067.jpg

real_00068.jpg -- img_00068.jpg

real_00069.jpg -- img_00069.jpg

real_00070.jpg -- img_00070.jpg

real_00071.jpg -- img_00071.jpg

real_00072.jpg -- img_00072.jpg

real_00074.jpg -- img_00074.jpg

real_00075.jpg -- img_00075.jpg

real_00076.jpg -- img_00076.jpg

real_00077.jpg -- img_00077.jpg

real_00078.jpg -- img_00078.jpg

real_00079.jpg -- img_00079.jpg

real_00080.jpg -- img_00080.jpg

real_00081.jpg -- img_00081.jpg

real_00082.jpg -- img_00082.jpg

real_00083.jpg -- img_00083.jpg

real_00084.jpg -- img_00084.jpg

real_00085.jpg -- img_00085.jpg

real_00087.jpg -- img_00087.jpg

real_00088.jpg -- img_00088.jpg

real_00089.jpg -- img_00089.jpg

real_00090.jpg -- img_00090.jpg

real_00091.jpg -- img_00091.jpg

real_00092.jpg -- img_00092.jpg

real_00093.jpg -- img_00093.jpg

real_00094.jpg -- img_00094.jpg

real_00095.jpg -- img_00095.jpg

real_00096.jpg -- img_00096.jpg

real_00097.jpg -- img_00097.jpg

real_00098.jpg -- img_00098.jpg

real_00100.jpg -- img_00100.jpg

real_00101.jpg -- img_00101.jpg

real_00102.jpg -- img_00102.jpg

real_00103.jpg -- img_00103.jpg

real_00104.jpg -- img_00104.jpg

real_00106.jpg -- img_00106.jpg

real_00107.jpg -- img_00107.jpg

real_00108.jpg -- img_00108.jpg

real_00109.jpg -- img_00109.jpg

real_00111.jpg -- img_00111.jpg

real_00113.jpg -- img_00113.jpg

##### 5. (fin del ejercicio)

## Ejercicio 1.c)  E:

##### 1. (regreso a la carpeta principal y verifico que lo hice correctamente)

. $cd ..

. $pwd

. >/c/Users/nombre/desktop/tp3-data-master

. $ls

. >fake/  README.md  real/

##### 2. (creo una carpeta de nombre "dataSet" y verifico que se halla creado)

. $mkdir dataSet

. $ls

. >dataSet/  fake/  README.md  real/

##### 3. (fin del ejercicio)


## Ejercicio 1.c)  F:

##### 1. (entro en la carpeta "fake", verifico que estoy ahí y que hay dentro)

. $cd ..

. $cd fake

. $pwd

. > /c/Users/nombre/desktop/tp3-data-master/fake

. $ls

. > img_279_1111.jpg  img_399_0011.jpg  img_434_1110.jpg  img_448_0011.jpg  img_460_1111.jpg  img_474_1110.jpg
img_297_0010.jpg  img_423_1101.jpg  img_435_1111.jpg  img_449_1111.jpg  img_461_1100.jpg  img_476_1111.jpg
img_309_1111.jpg  img_424_1111.jpg  img_436_1111.jpg  img_452_1111.jpg  img_462_1101.jpg  img_477_1100.jpg
img_317_1101.jpg  img_425_1111.jpg  img_437_1101.jpg  img_453_1111.jpg  img_464_1111.jpg  img_479_1111.jpg
img_318_1111.jpg  img_426_1111.jpg  img_441_0011.jpg  img_454_1111.jpg  img_466_1111.jpg  img_480_1111.jpg
img_320_1111.jpg  img_428_0001.jpg  img_442_0111.jpg  img_455_0101.jpg  img_467_1111.jpg  renameFake.dat
img_324_1111.jpg  img_429_1110.jpg  img_444_1111.jpg  img_456_1111.jpg  img_470_1001.jpg
img_343_1111.jpg  img_431_0001.jpg  img_446_0011.jpg  img_458_1111.jpg  img_471_1111.jpg
img_349_1111.jpg  img_432_1111.jpg  img_447_0011.jpg  img_459_1001.jpg  img_473_0011.jpg

##### 2. (como no quiero mover el directorio, sino lo que hay dentro, hago un "loop" y luego verifico que los archivos se hallan movido)

. $for f in img*

. $do

. $mv "$f" ../dataSet/"$f"

. $done

. $ls

. > renameFake.dat

##### 3. (muevo individualmente el archivo "renameFake.dat" y luego verifico que todos los archivos estén en la carpeta "dataSet")

. $mv renameFake.dat ../dataSet/renameFake.dat

. $cd ..

. $cd dataSet

. $ls

. > img_279_1111.jpg  img_399_0011.jpg  img_434_1110.jpg  img_448_0011.jpg  img_460_1111.jpg  img_474_1110.jpg
img_297_0010.jpg  img_423_1101.jpg  img_435_1111.jpg  img_449_1111.jpg  img_461_1100.jpg  img_476_1111.jpg
img_309_1111.jpg  img_424_1111.jpg  img_436_1111.jpg  img_452_1111.jpg  img_462_1101.jpg  img_477_1100.jpg
img_317_1101.jpg  img_425_1111.jpg  img_437_1101.jpg  img_453_1111.jpg  img_464_1111.jpg  img_479_1111.jpg
img_318_1111.jpg  img_426_1111.jpg  img_441_0011.jpg  img_454_1111.jpg  img_466_1111.jpg  img_480_1111.jpg
img_320_1111.jpg  img_428_0001.jpg  img_442_0111.jpg  img_455_0101.jpg  img_467_1111.jpg  renameFake.dat
img_324_1111.jpg  img_429_1110.jpg  img_444_1111.jpg  img_456_1111.jpg  img_470_1001.jpg
img_343_1111.jpg  img_431_0001.jpg  img_446_0011.jpg  img_458_1111.jpg  img_471_1111.jpg
img_349_1111.jpg  img_432_1111.jpg  img_447_0011.jpg  img_459_1001.jpg  img_473_0011.jpg

##### .5 (ahora hago lo mismo con la carpeta "real")

. $cd ..

. $cd real

. $ls

. >img_00061.jpg  img_00069.jpg  img_00077.jpg  img_00085.jpg  img_00093.jpg  img_00101.jpg  img_00111.jpg
img_00062.jpg  img_00070.jpg  img_00078.jpg  img_00086.jpg  img_00094.jpg  img_00102.jpg  img_00113.jpg
img_00063.jpg  img_00071.jpg  img_00079.jpg  img_00087.jpg  img_00095.jpg  img_00103.jpg  renameReal.dat
img_00064.jpg  img_00072.jpg  img_00080.jpg  img_00088.jpg  img_00096.jpg  img_00104.jpg
img_00065.jpg  img_00073.jpg  img_00081.jpg  img_00089.jpg  img_00097.jpg  img_00106.jpg
img_00066.jpg  img_00074.jpg  img_00082.jpg  img_00090.jpg  img_00098.jpg  img_00107.jpg
img_00067.jpg  img_00075.jpg  img_00083.jpg  img_00091.jpg  img_00099.jpg  img_00108.jpg
img_00068.jpg  img_00076.jpg  img_00084.jpg  img_00092.jpg  img_00100.jpg  img_00109.jpg

. $for f in img*

. $do

. $mv "$f" ../dataSet/"$f"

. $done

. $mv renameReal.dat   ../dataSet/renameReal.dat

. $ls

. >

##### 6. (ahora que los archivos no están en "real" comprobamos que tanto los archivos de "real" como los de "fake" estén en "dataSet")

. $cd ..

. $cd dataSet

. $ls

. > img_00061.jpg  img_00078.jpg  img_00095.jpg     img_297_0010.jpg  img_434_1110.jpg  img_459_1001.jpg
img_00062.jpg  img_00079.jpg  img_00096.jpg     img_309_1111.jpg  img_435_1111.jpg  img_460_1111.jpg
img_00063.jpg  img_00080.jpg  img_00097.jpg     img_317_1101.jpg  img_436_1111.jpg  img_461_1100.jpg
img_00064.jpg  img_00081.jpg  img_00098.jpg     img_318_1111.jpg  img_437_1101.jpg  img_462_1101.jpg
img_00065.jpg  img_00082.jpg  img_00099.jpg     img_320_1111.jpg  img_441_0011.jpg  img_464_1111.jpg
img_00066.jpg  img_00083.jpg  img_00100.jpg     img_324_1111.jpg  img_442_0111.jpg  img_466_1111.jpg
img_00067.jpg  img_00084.jpg  img_00101.jpg     img_343_1111.jpg  img_444_1111.jpg  img_467_1111.jpg
img_00068.jpg  img_00085.jpg  img_00102.jpg     img_349_1111.jpg  img_446_0011.jpg  img_470_1001.jpg
img_00069.jpg  img_00086.jpg  img_00103.jpg     img_399_0011.jpg  img_447_0011.jpg  img_471_1111.jpg
img_00070.jpg  img_00087.jpg  img_00104.jpg     img_423_1101.jpg  img_448_0011.jpg  img_473_0011.jpg
img_00071.jpg  img_00088.jpg  img_00106.jpg     img_424_1111.jpg  img_449_1111.jpg  img_474_1110.jpg
img_00072.jpg  img_00089.jpg  img_00107.jpg     img_425_1111.jpg  img_452_1111.jpg  img_476_1111.jpg
img_00073.jpg  img_00090.jpg  img_00108.jpg     img_426_1111.jpg  img_453_1111.jpg  img_477_1100.jpg
img_00074.jpg  img_00091.jpg  img_00109.jpg     img_428_0001.jpg  img_454_1111.jpg  img_479_1111.jpg
img_00075.jpg  img_00092.jpg  img_00111.jpg     img_429_1110.jpg  img_455_0101.jpg  img_480_1111.jpg
img_00076.jpg  img_00093.jpg  img_00113.jpg     img_431_0001.jpg  img_456_1111.jpg  renameFake.dat
img_00077.jpg  img_00094.jpg  img_279_1111.jpg  img_432_1111.jpg  img_458_1111.jpg  renameReal.dat

##### 7. (fin del ejercicio) 


## Ejercicio 1.c)  G:

El comando "tar" es una herramienta que permite comprimir archivos fácil y rápidamente.
Tar significa “Tape Archive” en inglés, lo que en español sería archivo de cinta de grabación, y se utiliza para comprimir una colección de archivos y carpetas. Consta de algunas opciones que se agregan después del comando de la siguiente manera:

- "-v"	Ejecuta el comando en modo detallado para mostrar el progreso del archivo.
- "-f"	Especifica el nombre del archivo.
- "-W" Se utiliza para verificar un archivo comprimido.
- "-t"	Se usa para visualizar el contenido del archivo.
- "-c"	Crea un nuevo archivo que contiene los elementos especificados.
- "-r"	Usado para agregar o actualizar archivos o directorios al archivo existente.
- "-x"	Extrae el archivo de almacenamiento en el disco.

Cuenta con mas opciones pero, para nuestros fines, nos bastará con combinar estas.
Para crear un archivo comprimido de la carpeta "dataSet" obtenida en los ejercicios anteriores proseguimos de la siguiente manera:

##### 1. (Nos posicionamos fuera de la carpeta para poder referenciarla)

. $pwd

. >/c/Users/nombre/desktop/tp3-data-master/dataSet

. $cd ..

. $ls

. > dataSet/  fake/  README.md  real/

##### 2. (escribimos el comando en combinación "-cvf" antes que "tar". La "c" crear un nuevo archivo ".tar", la "v" muestra una descripción detallada del progreso de la compresión y la "f" da nombre al archivo; Al poner esta combinación, nos aseguramos de hacer todo lo que las mismas funciones hacen individualmente; luego ponemos el nombre del archivo comprimido seguido de la carpeta que deseamos comprimir que, en este caso, es "dataSet" )

. $tar -cvf BackupdataSet   dataSet

. >dataSet/
dataSet/img_00061.jpg
dataSet/img_00062.jpg
dataSet/img_00063.jpg
dataSet/img_00064.jpg
dataSet/img_00065.jpg
dataSet/img_00066.jpg
dataSet/img_00067.jpg
dataSet/img_00068.jpg
dataSet/img_00069.jpg
dataSet/img_00070.jpg
dataSet/img_00071.jpg
dataSet/img_00072.jpg
dataSet/img_00073.jpg
dataSet/img_00074.jpg
dataSet/img_00075.jpg
dataSet/img_00076.jpg
dataSet/img_00077.jpg
dataSet/img_00078.jpg
dataSet/img_00079.jpg
dataSet/img_00080.jpg
dataSet/img_00081.jpg
dataSet/img_00082.jpg
dataSet/img_00083.jpg
dataSet/img_00084.jpg
dataSet/img_00085.jpg
dataSet/img_00086.jpg
dataSet/img_00087.jpg
dataSet/img_00088.jpg
dataSet/img_00089.jpg
dataSet/img_00090.jpg
dataSet/img_00091.jpg
dataSet/img_00092.jpg
dataSet/img_00093.jpg
dataSet/img_00094.jpg
dataSet/img_00095.jpg
dataSet/img_00096.jpg
dataSet/img_00097.jpg
dataSet/img_00098.jpg
dataSet/img_00099.jpg
dataSet/img_00100.jpg
dataSet/img_00101.jpg
dataSet/img_00102.jpg
dataSet/img_00103.jpg
dataSet/img_00104.jpg
dataSet/img_00106.jpg
dataSet/img_00107.jpg
dataSet/img_00108.jpg
dataSet/img_00109.jpg
dataSet/img_00111.jpg
dataSet/img_00113.jpg
dataSet/img_279_1111.jpg
dataSet/img_297_0010.jpg
dataSet/img_309_1111.jpg
dataSet/img_317_1101.jpg
dataSet/img_318_1111.jpg
dataSet/img_320_1111.jpg
dataSet/img_324_1111.jpg
dataSet/img_343_1111.jpg
dataSet/img_349_1111.jpg
dataSet/img_399_0011.jpg
dataSet/img_423_1101.jpg
dataSet/img_424_1111.jpg
dataSet/img_425_1111.jpg
dataSet/img_426_1111.jpg
dataSet/img_428_0001.jpg
dataSet/img_429_1110.jpg
dataSet/img_431_0001.jpg
dataSet/img_432_1111.jpg
dataSet/img_434_1110.jpg
dataSet/img_435_1111.jpg
dataSet/img_436_1111.jpg
dataSet/img_437_1101.jpg
dataSet/img_441_0011.jpg
dataSet/img_442_0111.jpg
dataSet/img_444_1111.jpg
dataSet/img_446_0011.jpg
dataSet/img_447_0011.jpg
dataSet/img_448_0011.jpg
dataSet/img_449_1111.jpg
dataSet/img_452_1111.jpg
dataSet/img_453_1111.jpg
dataSet/img_454_1111.jpg
dataSet/img_455_0101.jpg
dataSet/img_456_1111.jpg
dataSet/img_458_1111.jpg
dataSet/img_459_1001.jpg
dataSet/img_460_1111.jpg
dataSet/img_461_1100.jpg
dataSet/img_462_1101.jpg
dataSet/img_464_1111.jpg
dataSet/img_466_1111.jpg
dataSet/img_467_1111.jpg
dataSet/img_470_1001.jpg
dataSet/img_471_1111.jpg
dataSet/img_473_0011.jpg
dataSet/img_474_1110.jpg
dataSet/img_476_1111.jpg
dataSet/img_477_1100.jpg
dataSet/img_479_1111.jpg
dataSet/img_480_1111.jpg
dataSet/renameFake.dat
dataSet/renameReal.dat

##### 3. (comprobamos que el archivo se halla creado y luego vemos que hay dentro con la opción "-tvf")

. $ls

. >BackupdataSet.tar  dataSet/  fake/  README.md  real/

.$tar -tvf BackupdataSet.tar

. >drwxr-xr-x Gaston/197121     0 2020-06-08 19:41 dataSet/
-rw-r--r-- Gaston/197121 125932 2020-05-11 23:40 dataSet/img_00061.jpg
-rw-r--r-- Gaston/197121 106008 2020-05-11 23:40 dataSet/img_00062.jpg
-rw-r--r-- Gaston/197121 118849 2020-05-11 23:40 dataSet/img_00063.jpg
-rw-r--r-- Gaston/197121  70802 2020-05-11 23:40 dataSet/img_00064.jpg
-rw-r--r-- Gaston/197121 106917 2020-05-11 23:40 dataSet/img_00065.jpg
-rw-r--r-- Gaston/197121  95598 2020-05-11 23:40 dataSet/img_00066.jpg
-rw-r--r-- Gaston/197121  80337 2020-05-11 23:40 dataSet/img_00067.jpg
-rw-r--r-- Gaston/197121 122429 2020-05-11 23:40 dataSet/img_00068.jpg
-rw-r--r-- Gaston/197121 124200 2020-05-11 23:40 dataSet/img_00069.jpg
-rw-r--r-- Gaston/197121 128122 2020-05-11 23:40 dataSet/img_00070.jpg
-rw-r--r-- Gaston/197121  57354 2020-05-11 23:40 dataSet/img_00071.jpg
-rw-r--r-- Gaston/197121 107081 2020-05-11 23:40 dataSet/img_00072.jpg
-rw-r--r-- Gaston/197121 121945 2020-05-11 23:40 dataSet/img_00073.jpg
-rw-r--r-- Gaston/197121  99496 2020-05-11 23:40 dataSet/img_00074.jpg
-rw-r--r-- Gaston/197121 147406 2020-05-11 23:40 dataSet/img_00075.jpg
-rw-r--r-- Gaston/197121  82587 2020-05-11 23:40 dataSet/img_00076.jpg
-rw-r--r-- Gaston/197121 149353 2020-05-11 23:40 dataSet/img_00077.jpg
-rw-r--r-- Gaston/197121 160309 2020-05-11 23:40 dataSet/img_00078.jpg
-rw-r--r-- Gaston/197121 121133 2020-05-11 23:40 dataSet/img_00079.jpg
-rw-r--r-- Gaston/197121 154201 2020-05-11 23:40 dataSet/img_00080.jpg
-rw-r--r-- Gaston/197121  94979 2020-05-11 23:40 dataSet/img_00081.jpg
-rw-r--r-- Gaston/197121 105805 2020-05-11 23:40 dataSet/img_00082.jpg
-rw-r--r-- Gaston/197121 116791 2020-05-11 23:40 dataSet/img_00083.jpg
-rw-r--r-- Gaston/197121 119670 2020-05-11 23:40 dataSet/img_00084.jpg
-rw-r--r-- Gaston/197121  79263 2020-05-11 23:40 dataSet/img_00085.jpg
-rw-r--r-- Gaston/197121 121051 2020-05-11 23:40 dataSet/img_00086.jpg
-rw-r--r-- Gaston/197121 115099 2020-05-11 23:40 dataSet/img_00087.jpg
-rw-r--r-- Gaston/197121 106106 2020-05-11 23:40 dataSet/img_00088.jpg
-rw-r--r-- Gaston/197121  67703 2020-05-11 23:40 dataSet/img_00089.jpg
-rw-r--r-- Gaston/197121  80517 2020-05-11 23:40 dataSet/img_00090.jpg
-rw-r--r-- Gaston/197121 102293 2020-05-11 23:40 dataSet/img_00091.jpg
-rw-r--r-- Gaston/197121  81152 2020-05-11 23:40 dataSet/img_00092.jpg
-rw-r--r-- Gaston/197121 127821 2020-05-11 23:40 dataSet/img_00093.jpg
-rw-r--r-- Gaston/197121  78478 2020-05-11 23:40 dataSet/img_00094.jpg
-rw-r--r-- Gaston/197121 101342 2020-05-11 23:40 dataSet/img_00095.jpg
-rw-r--r-- Gaston/197121  89970 2020-05-11 23:40 dataSet/img_00096.jpg
-rw-r--r-- Gaston/197121  60631 2020-05-11 23:40 dataSet/img_00097.jpg
-rw-r--r-- Gaston/197121  88630 2020-05-11 23:40 dataSet/img_00098.jpg
-rw-r--r-- Gaston/197121  84679 2020-05-11 23:40 dataSet/img_00099.jpg
-rw-r--r-- Gaston/197121  82600 2020-05-11 23:40 dataSet/img_00100.jpg
-rw-r--r-- Gaston/197121  63218 2020-05-11 23:40 dataSet/img_00101.jpg
-rw-r--r-- Gaston/197121  88994 2020-05-11 23:40 dataSet/img_00102.jpg
-rw-r--r-- Gaston/197121 133876 2020-05-11 23:40 dataSet/img_00103.jpg
-rw-r--r-- Gaston/197121 134715 2020-05-11 23:40 dataSet/img_00104.jpg
-rw-r--r-- Gaston/197121  93465 2020-05-11 23:40 dataSet/img_00106.jpg
-rw-r--r-- Gaston/197121 122611 2020-05-11 23:40 dataSet/img_00107.jpg
-rw-r--r-- Gaston/197121  87762 2020-05-11 23:40 dataSet/img_00108.jpg
-rw-r--r-- Gaston/197121 113678 2020-05-11 23:40 dataSet/img_00109.jpg
-rw-r--r-- Gaston/197121 101439 2020-05-11 23:40 dataSet/img_00111.jpg
-rw-r--r-- Gaston/197121 102700 2020-05-11 23:40 dataSet/img_00113.jpg
-rw-r--r-- Gaston/197121  76678 2020-05-11 23:40 dataSet/img_279_1111.jpg
-rw-r--r-- Gaston/197121  81860 2020-05-11 23:40 dataSet/img_297_0010.jpg
-rw-r--r-- Gaston/197121  95272 2020-05-11 23:40 dataSet/img_309_1111.jpg
-rw-r--r-- Gaston/197121  58340 2020-05-11 23:40 dataSet/img_317_1101.jpg
-rw-r--r-- Gaston/197121  40612 2020-05-11 23:40 dataSet/img_318_1111.jpg
-rw-r--r-- Gaston/197121  56924 2020-05-11 23:40 dataSet/img_320_1111.jpg
-rw-r--r-- Gaston/197121  67929 2020-05-11 23:40 dataSet/img_324_1111.jpg
-rw-r--r-- Gaston/197121  85376 2020-05-11 23:40 dataSet/img_343_1111.jpg
-rw-r--r-- Gaston/197121  40386 2020-05-11 23:40 dataSet/img_349_1111.jpg
-rw-r--r-- Gaston/197121 134806 2020-05-11 23:40 dataSet/img_399_0011.jpg
-rw-r--r-- Gaston/197121  79825 2020-05-11 23:40 dataSet/img_423_1101.jpg
-rw-r--r-- Gaston/197121 116376 2020-05-11 23:40 dataSet/img_424_1111.jpg
-rw-r--r-- Gaston/197121 102038 2020-05-11 23:40 dataSet/img_425_1111.jpg
-rw-r--r-- Gaston/197121  89657 2020-05-11 23:40 dataSet/img_426_1111.jpg
-rw-r--r-- Gaston/197121  74626 2020-05-11 23:40 dataSet/img_428_0001.jpg
-rw-r--r-- Gaston/197121 100684 2020-05-11 23:40 dataSet/img_429_1110.jpg
-rw-r--r-- Gaston/197121  88084 2020-05-11 23:40 dataSet/img_431_0001.jpg
-rw-r--r-- Gaston/197121 134433 2020-05-11 23:40 dataSet/img_432_1111.jpg
-rw-r--r-- Gaston/197121 143169 2020-05-11 23:40 dataSet/img_434_1110.jpg
-rw-r--r-- Gaston/197121 102366 2020-05-11 23:40 dataSet/img_435_1111.jpg
-rw-r--r-- Gaston/197121  92497 2020-05-11 23:40 dataSet/img_436_1111.jpg
-rw-r--r-- Gaston/197121 106113 2020-05-11 23:40 dataSet/img_437_1101.jpg
-rw-r--r-- Gaston/197121  95484 2020-05-11 23:40 dataSet/img_441_0011.jpg
-rw-r--r-- Gaston/197121  81420 2020-05-11 23:40 dataSet/img_442_0111.jpg
-rw-r--r-- Gaston/197121 166433 2020-05-11 23:40 dataSet/img_444_1111.jpg
-rw-r--r-- Gaston/197121  98180 2020-05-11 23:40 dataSet/img_446_0011.jpg
-rw-r--r-- Gaston/197121 170882 2020-05-11 23:40 dataSet/img_447_0011.jpg
-rw-r--r-- Gaston/197121 120117 2020-05-11 23:40 dataSet/img_448_0011.jpg
-rw-r--r-- Gaston/197121 100861 2020-05-11 23:40 dataSet/img_449_1111.jpg
-rw-r--r-- Gaston/197121  93244 2020-05-11 23:40 dataSet/img_452_1111.jpg
-rw-r--r-- Gaston/197121 100728 2020-05-11 23:40 dataSet/img_453_1111.jpg
-rw-r--r-- Gaston/197121  80598 2020-05-11 23:40 dataSet/img_454_1111.jpg
-rw-r--r-- Gaston/197121 117500 2020-05-11 23:40 dataSet/img_455_0101.jpg
-rw-r--r-- Gaston/197121  93848 2020-05-11 23:40 dataSet/img_456_1111.jpg
-rw-r--r-- Gaston/197121 174496 2020-05-11 23:40 dataSet/img_458_1111.jpg
-rw-r--r-- Gaston/197121 128023 2020-05-11 23:40 dataSet/img_459_1001.jpg
-rw-r--r-- Gaston/197121 128173 2020-05-11 23:40 dataSet/img_460_1111.jpg
-rw-r--r-- Gaston/197121 126778 2020-05-11 23:40 dataSet/img_461_1100.jpg
-rw-r--r-- Gaston/197121 116198 2020-05-11 23:40 dataSet/img_462_1101.jpg
-rw-r--r-- Gaston/197121  67345 2020-05-11 23:40 dataSet/img_464_1111.jpg
-rw-r--r-- Gaston/197121 150925 2020-05-11 23:40 dataSet/img_466_1111.jpg
-rw-r--r-- Gaston/197121 117745 2020-05-11 23:40 dataSet/img_467_1111.jpg
-rw-r--r-- Gaston/197121 141151 2020-05-11 23:40 dataSet/img_470_1001.jpg
-rw-r--r-- Gaston/197121 136030 2020-05-11 23:40 dataSet/img_471_1111.jpg
-rw-r--r-- Gaston/197121  81445 2020-05-11 23:40 dataSet/img_473_0011.jpg
-rw-r--r-- Gaston/197121 103028 2020-05-11 23:40 dataSet/img_474_1110.jpg
-rw-r--r-- Gaston/197121 133442 2020-05-11 23:40 dataSet/img_476_1111.jpg
-rw-r--r-- Gaston/197121 108742 2020-05-11 23:40 dataSet/img_477_1100.jpg
-rw-r--r-- Gaston/197121 101879 2020-05-11 23:40 dataSet/img_479_1111.jpg
-rw-r--r-- Gaston/197121 108255 2020-05-11 23:40 dataSet/img_480_1111.jpg
-rw-r--r-- Gaston/197121   1850 2020-06-08 18:26 dataSet/renameFake.dat
-rw-r--r-- Gaston/197121   1600 2020-06-08 18:29 dataSet/renameReal.dat

##### 4. (como vemos, el archivo fue creado y el contenido del mismo esta correctamente comprimido; fin del ejercicio)